export const FETCHING_DATA = 'FETCHING_DATA'
export const FETCHING_DATA_SUCCESS = 'FETCHING_DATA_SUCCESS'
export const FETCHING_DATA_FAILURE = 'FETCHING_DATA_FAILURE'

export const API_KEY = '290586-TasteDiv-FQPXTFI5'
export const BASE_URL = 'https://tastedive.com/api/'


export const SET_INPUT_DATA = 'SET_INPUT_DATA'
export const SET_CATEGORY_DATA = 'SET_CATEGORY_DATA'

export const TOGGLE_MODAL = 'TOGGLE_MODAL'
export const SELECTED_ITEM = 'SELECTED_ITEM'
