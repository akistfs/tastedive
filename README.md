# README #

TasteDive is a simple implementation of the TasteDive API

### What the TasteDive API does ###

* Provide recommendations and help you discover new music, movies, TV shows, books, authors and games, based on what you like.


### Made with ###

* React native
* Redux
* Redux-Saga